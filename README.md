<div align="center">
<img src="docs/media/elk-logo.webp" width="300"/>
</div>


<br>
</br>

___

# **Stack ELK**

Cette stack a été déployé sur Azure dans le cadre d'une mise en situation professionnel avec un budget limité.

## **Summary**

[[*TOC*]]

### **Topologie**

<img src="docs/media/topo-infra.png " width="600"/>

La collecte des logs et des metrics est réalisé avec Filebeat et Metricbeat 

<img src="docs/media/topo-metric.png " width="600"/>

___
### Installation manuel d'Elasticsearch et Kibana
___

- #### [Setup](docs/setup-elastic.md)
- #### [Config](docs/config-elastic.md)
- #### [Encrypt](docs/encrypt-elastic.md)
- #### [Beats](doc/setup-beats.md)

___
### Installation automatisé d'Elasticsearch et Kibana
___

- #### **Lab Virtualbox**
___

Sur virtualbox créer l'interface réseau

<img src="docs/media/network-virtualbox.png " width="600"/>

- #### **Installer Ansible**

    ```bash
    apt install ansible
    ```

- #### **Installer Vagrant**

    - https://developer.hashicorp.com/vagrant/install


    - Plugins
        - VirtualBox:
            ```bash
            vagrant plugin install vagrant-vbguest
            ```


- #### **Personnaliser le fichier vagrant**
___

- **sur virtualbox**
```bash
vagrant/virtualbox/vagrantfile
```
vous pourrez y definir l'ip, les ressources... ainsi que le path du script si vous souhaitez définir votre propre script.  
*par default l'ip utilisé pour 2 noeuds commence a 192.168.56.3# +i*

- #### **Personnaliser le fichier all.yml et hosts**
___

```bash
playbook/group_vars/all.yml
inventory/hosts
```
Lors du **"make run"** vous serez invité à définir les variables qui rempliront **automatiquement** le fichier **"all.yml"**.  
**Par contre le fichier hosts doit etre défini avant.**  
**ATTENTION !!** le nommage des hosts dois correspondre avec le nom des assets.

- #### **Install & Run with Make**
___

Personnaliser le fichier **Makefile** pour definir l'utilisateur et ip des serveur pour la partie ssh
```bash
ssh-copy-id -i ~/.ssh/id_ed25519.pub user@ip-srv
```

- **Install make**
    ```bash
    apt install make -y
    ```
- **Créer les vms**
    ```bash
    make lab-virtualbox
    ```
- **Executer Ansible**
    ```bash
    make run
    ```
- **Clean le Lab**
    ```bash
    make clean
    ```
___
L'interface kibana est accessible sur https://votre-ip:5601  
L'api elasticsearch est joignable sur https://votre-ip:9200  
L'utilisateur défini par défault est elastic mp: iopiop  
___

notes:  
- activer le role secu
```bash
/usr/share/kibana/bin/kibana-encryption-keys generate
```
editer kibana.yml pour enregistrer les clés

- crééer un user avec la commande elastic
```bash
/usr/share/elasticsearch/bin/elasticsearch-users useradd kevin -p changeme -r superuser
```

- Rule trigger:
```bash
sudo tail -f /var/log/suricata/fast.log
curl http://testmynids.org/uid/index.html
```

-- Fleet server:
```bash
--fleet-server-es-ca=/etc/elasticsearch/certs/elasticsearch-ca.pem \
```