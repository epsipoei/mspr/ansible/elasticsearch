.PHONY: lab-kvm lab-virtualbox run clean

lab-kvm:
		cd vagrant/kvm && \
		vagrant up 
		echo "" > ~/.ssh/known_hosts
		ssh-copy-id -i ~/.ssh/id_ed25519.pub debian@192.168.56.30
		ssh-copy-id -i ~/.ssh/id_ed25519.pub debian@192.168.56.31

lab-virtualbox:
		cd vagrant/virtualbox && \
		vagrant up
		echo "" > ~/.ssh/known_hosts
		ssh-copy-id -i ~/.ssh/id_ed25519.pub debian@192.168.56.30
		ssh-copy-id -i ~/.ssh/id_ed25519.pub debian@192.168.56.31

run:
		./scripts/main.sh
		ansible-playbook -i inventory playbook/elasticsearch.yml
		./scripts/clean.sh
clean:
		(rm /elastic_certs/elastic-stack-ca.p12 || true)
		(rm /elastic_certs/elasticsearch-ca.pem || true)
		cd vagrant/kvm && \
		(vagrant destroy || true)
		cd vagrant/virtualbox && \
		(vagrant destroy || true)
			