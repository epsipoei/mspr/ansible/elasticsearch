# Elasticsearch - Kibana

## **Summary**

[[*TOC*]]

### Elasticsearch

#### **Ajouter les options JVM à elasticsearch**

dans le fichier
```bash
/etc/elasticsearch/jvm.options
```
ajouter les lignes suivante afin de reduire les besoins en RAM:
```bash
-Xms512m
-Xmx512m
```
#### **Edit elasticsearch.yml**

```bash
/etc/elasticsearch/elasticsearch.yml
```
Dans ce fichier [elasticsearch.yml](yml/elastic.yml.md) nous allons indiquer:
- le nom du cluster
- le role du ou des nodes
- le chemin du repertoire ou seront enregistré les données
- le chemin du repertoire ou seront journalisé les logs
- l'ip exposé du noeud sur le reseau
- le port
- la decouverte des serveurs du cluster
- les options de sécurité

### Kibana

#### **Edit kibana.yml**
```bash
/etc/kibana/kibana.yml
```
[kibana.yml](yml/kibana.yml.md)

### Creer le repertoire de log

```bash
mkdir /var/log/kibana
chown root:kibana /var/log/kibana
chmod 770 /var/log/kibana
```

### **[Return](../README.md)**