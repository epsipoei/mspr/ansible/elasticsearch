# **Elasticsearch**

## **Summary**

[[*TOC*]]

### **Import the Elasticsearch PGP Key**

```bash
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
```

### **Installing from the APT repository**
```bash
sudo apt-get install apt-transport-https
```
```bash
echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list
```
```bash
sudo apt-get update && sudo apt-get install elasticsearch kibana
```
### **[Return](../README.md)**