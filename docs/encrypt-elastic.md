# **Set up basic security for the Elastic Stack plus secured HTTPS traffic**

## **Summary**

[[*TOC*]]

### Elasticsearch

#### **Référence**
* <a href="https://www.elastic.co/guide/en/elasticsearch/reference/8.13/security-basic-setup.html">elastic_security</a>

#### **generate ca**

```bash
/usr/share/elasticsearch/bin/elasticsearch-certutil ca
```

#### **generate cert**

```bash
/usr/share/elasticsearch/bin/elasticsearch-certutil cert --ca elastic-stack-ca.p12
```

#### **generate a Certificate Signing Request (CSR)**

```bash
/usr/share/elasticsearch/bin/elasticsearch-certutil http
```

dezipper le fichier qui contient les fichier http.p12 et elasticsearch-ca.pem

```bash
cp elasticsearch/http.p12 /etc/elasticsearch/certs/http.p12
cp kibana/elasticsearch-ca.pem /etc/kibana/elasticsearch-ca.pem
```

#### **Add the password for your private key to the secure settings in Elasticsearch.**
On enregistre les passwords defini lors des etapes precedente
```bash
/usr/share/elasticsearch/bin/elasticsearch-keystore add -x -f xpack.security.transport.ssl.keystore.secure_password
```
```bash
/usr/share/elasticsearch/bin/elasticsearch-keystore add -x -f xpack.security.transport.ssl.truststore.secure_password
```
```bash
/usr/share/elasticsearch/bin/elasticsearch-keystore add -x -f xpack.security.http.ssl.keystore.secure_password
```
```bash
/usr/share/elasticsearch/bin/elasticsearch-keystore add -x -f bootstrap.password
```
#### **generating random password**
Password qui serviront pour configurer les authentifications
```bash
/usr/share/elasticsearch/bin/elasticsearch-setup-passwords auto
```

### Kibana

#### **generate kibana https cert**
```bash
openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes
        -keyout /etc/kibana/kibana_server.key -out /etc/kibana/kibana_server.crt -subj "/CN=local.com"
        -addext "subjectAltName=IP:127.0.0.1"
```
```bash
chmod 644 /etc/kibana/kibana_server.crt
chmod 644 /etc/kibana/kibana_server.key
```
```bash
chown root:kibana /etc/kibana/kibana_server.crt
chown root:kibana /etc/kibana/kibana_server.key
```
### **[Return](../README.md)**