# **Filebeat and Metricbeat**

## **Summary**

[[*TOC*]]

### **Installation des paquets**

```bash
apt install -y filebeat metricbeat
```

### **Fix and create directory permission**
```bash
#!/bin/bash

directories=(
    "/etc/filebeat"
    "/var/log/filebeat"
    "/etc/filebeat/certs"
    "/etc/metricbeat"
    "/var/log/metricbeat"
    "/etc/metricbeat/certs"
)

for directory in "${directories[@]}"; do
    sudo mkdir -p "$directory" && sudo chown root:root "$directory" && sudo chmod u=rwX,g=rX,o-rwx "$directory"
done
```

### **Copy elastic CA**

```bash
cp /etc/elasticsearch/certs/elasticsearch-ca.pem /etc/filebeat/certs/elasticsearch-ca.pem
```
```bash
cp /etc/elasticsearch/certs/elasticsearch-ca.pem /etc/metricbeat/certs/elasticsearch-ca.pem
```

### **Configuartion de filebeat et metricbeat**

* [filebeat.yml](yml/filebeat.yml.md)
* [metricbeat.yml](yml/metricbeat.yml.md)

### **Les modules**

* Filebeat:
```bash
filebeat modules enable elasticsearch
filebeat modules enable kibana
```
* Metricbeat:
```bash
metricbeat modules enable elasticsearch
metricbeat modules enable kibana-xpack
```
Puis on va editer:
```bash
/etc/metricbeat/module.d/elasticsearch.yml
```
module [elasticsearch.yml](yml/module-metric-elactic.yml.md)

```bash
/etc/metricbeat/module.d/kibana-xpack.yml
```
module [kibana-xpack.yml](yml/module-metric-kibana.yml.md)

### **Setup modules**
```bash
filebeat setup
```
```bash
metricbeat setup
```
### **Enable and start services**
```bash
systemctl enable filebeat
systemctl enable metricbeat
```
```bash
systemctl start filebeat
systemctl start metricbeat
```
### **[Return](../README.md)**