# module elasticsearch.yml
## **[Return](../README.md)**

```yml
# Module: kibana
# Docs: https://www.elastic.co/guide/en/beats/metricbeat/main/metricbeat-module-kibana.html

- module: kibana
  xpack.enabled: true
  period: 10s
  hosts:
    - "https://{{ kibana_ip_slave }}:5601"
    - "https://{{ kibana_ip_master }}:5601"

  username: "{{ beats_username }}"
  password: "{{ beats_password }}"
  ssl.verification_mode: "none"

```