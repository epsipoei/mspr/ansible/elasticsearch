# module elasticsearch.yml
## **[Return](../README.md)**

```yml
# Module: elasticsearch
# Docs: https://www.elastic.co/guide/en/beats/metricbeat/main/metricbeat-module-elasticsearch.html

- module: elasticsearch
  service.name: elastic_metricbeat_monitor
  xpack.enabled: true
  metricsets:
    - node
    - node_stats
    - index
    - index_recovery
    - index_summary
    - shard

  period: 10s
  hosts:
    - "https://{{ elasticsearch_ip_data }}:9200"
    - "https://{{ elasticsearch_ip_master }}:9200"

  username: "username"
  password: "password"
  ssl.certificate_authorities: ["/etc/metricbeat/certs/elasticsearch-ca.pem"]

```